/* Nom : MOUAMMAR
Prenom : SOULE
TP4_EXO_11 : Groupe 2

 === Les reponses à léexercies 11, se trouve dans ce fichier
  ci-dessous ==*/

drop DATABASE if EXISTS vue;

create database if not EXISTS vue;
use vue;



/*

REPONSE 1: CREATION DES TABLES 
  ==============================================================
            Table : PILOTE                                     
  ==============================================================*/

DROP TABLE IF EXISTS pilote;
CREATE TABLE IF NOT EXISTS pilote (
    numpil int not null UNIQUE,
    nompil varchar(35) not null
) ENGINE=INNODB;


/*==============================================================
             Table : AVION                                     
==============================================================*/
drop table if EXISTS avion;
create table if not EXISTS avion(
    numav int not null UNIQUE,
    nomavion varchar(20) not null
)ENGINE=INNODB;


/*==============================================================
            Table : VOL                                      
==============================================================*/
drop table if EXISTS vol;
create table if not EXISTS vol(
    numvol int not null UNIQUE,
    numpil int not null ,
    numav int not null ,
    departvol date,
    dureevol decimal(6,2)
)ENGINE=INNODB;


/* Ajout des clées Primaire aux tables */

alter table pilote
add constraint PK_pilote primary key (numpil);


alter table avion
add constraint PK_avion primary key (numav);


alter table vol
add constraint PK_vol primary key (numvol);


/* Insertions des données dans les tabmes */
insert into pilote(numpil, nompil) VALUES(1,'Tim'),(2, 'John');

insert into avion
    (numav, nomavion) 
    VALUES (1, 'A320'), (2, 'A330');


/* Ajout es clées etrangeres sur la table vol*/

alter table vol
   add constraint FK_vol foreign key (numpil)
      references pilote (numpil);

alter table vol
   add constraint FK_vol_avion foreign key (numav)
      references avion(numav);




/* insetions de donnée a la table vol*/

insert into vol(numvol,numpil, numav, departvol,dureevol)
    VALUES (1,2,1,'2017-01-15',3.5);


/* ajout des données des attriut dans les tables */

alter table avion 
   add localisav varchar(20),
   add capamaxav int not null;
   

   alter table vol 
  add  vildepvol varchar(20) ,
  add  vilarrvol varchar(20);

alter table pilote 
  add  vilpil varchar(20) ,
  add  salairepil numeric (10,2) ;

/* EXO 11_===== LES VUES STOCKEES === 

    une view permet e ne pas repeter une requette tout
    le temps ou de le devoir repeter, on peux le sauvagarder
    et utilser le view
    11.1.1_Creation de la view pilote et vol */

    CREATE view pilotesAvecVol 
    as 
    select DISTINCT numpil from pilote inner join vol USING(numpil);

   /* 11.1.2 utilsation de la view 
    Listons les numeros des Pilotes ayant volé*/

   select *from pilotesAvecVol order by numpil;
   


   /* Listons les pilotes ayant volées */

    select *from pilote where  numpil in 
    (select numpil from pilotesAvecVol) order by numpil;

    /*Affichage Console 

    
        +--------+--------+--------+------------+
        | numpil | nompil | vilpil | salairepil |
        +--------+--------+--------+------------+
        |      2 | John   | NULL   |       NULL |
        +--------+--------+--------+------------+
        1 row in set (0.01 sec)*/




















