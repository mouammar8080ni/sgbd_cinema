/* Nom: MOUAMMAR 
   Prenom :SOULE
   Classe : L2 Info
    TP3 : Grpe 2
    Date : 30/11/2022*/
CREATE DATABASE cinema; 
 use cinema;

/* ====  PARTIE 2.1 utilisations de jointure interne 'INNER JOIN' ====

    REPONSE 1 :Listons les noms des realisateurs et les titres 
    classé par nom des realisateurs*/

select realisateur.nom_real,film.titre_film from realisateur 
 inner join film where realisateur.num_real = film.num_real ORDER by realisateur.nom_real ASC;

/*Affichage Console :
+--------------------+------------------------------+
| nom_real           | titre_film                   |
+--------------------+------------------------------+
| Andreas Dresen     | Le septieme ciel             |
| Anna Novion        | Les grandes personnes        |
| Antonello Grimaldi | Caos Calmo                   |
| Ben Stiller        | Tonnere sous les tropiques   |
| Claude Pinoteau    | La boom                      |
| Clint Eastwood     | L'?hange                     |
| Edward Zwick       | The Last Samurai             |
| Georges Lucas      | Indianna jones4              |
| Georges Lucas      | red Tails                    |
| Georges Lucas      | Star wars                    |
| Hong Sang-soo      | Woman on the beach           |
| Pascal Thomas      | Le crime est notre affaire   |
| RenÃ© FÃ©ret       | Comme une ?oile dans la nuit |
| Sylvie Verheyde    | Stella                       |
| Ursula Meier       | Home                         |
+--------------------+------------------------------+
15 rows in set (0.00 sec) */

/* REPONSE 2 : Listons les titres et les films
        et les roles des acteurs qui on joué dans ces films, 
        classé par titre et nom*/

select film.titre_film,acteur.nom_act, jouer.role_jouer 
    from jouer 
     inner join film on film.num_film = jouer.num_film
      inner join acteur on acteur.num_act = jouer.num_act
            ORDER by film.titre_film , acteur.nom_act ASC;

/* Affichage Console :
    -> 
+------------------------------+-----------------------+------------+
| titre_film                   | nom_act               | role_jouer |
+------------------------------+-----------------------+------------+
| Caos Calmo                   | Alessandro Gassman    | second     |
| Caos Calmo                   | Nanni Moretti         | apparition |
| Caos Calmo                   | Valeria Golino        | premier    |
| Comme une ?oile dans la nuit | Nicolas Giraud        | apparition |
| Comme une ?oile dans la nuit | Salomé Stéverin       | second     |
| Comme une ?oile dans la nuit | Tom Cruise            | premier    |
| Home                         | Isabelle Huppert      | apparition |
| Home                         | Jean-Pierre Darrousin | second     |
| Home                         | Olivier Gourmet       | premier    |
| L'?hange                     | Angelina Jolie        | premier    |
| L'?hange                     | John Malkovich        | second     |
| La boom                      | Sophie Marceau        | premier    |
| Le crime est notre affaire   | André Dussolier       | apparition |
| Le crime est notre affaire   | Annie Cordy           | premier    |
| Le crime est notre affaire   | Catherine Frot        | second     |
| Le crime est notre affaire   | Jean-Pierre Darrousin | second     |
| Le septieme ciel             | Angelina Jolie        | premier    |
| Le septieme ciel             | Horst Westphal        | second     |
| Le septieme ciel             | Ursula Werner         | premier    |
| Les grandes personnes        | Jean-Pierre Darrousin | second     |
| Les grandes personnes        | Judith Henry          | apparition |
| Stella                       | Benjamin Biolay       | apparition |
| Stella                       | Guillaume Depardieu   | premier    |
| Stella                       | Judith Henry          | apparition |
| Stella                       | Karole Rocher         | premier    |
| Stella                       | Leora Barbara         | apparition |
| Stella                       | Melissa Rodrigues     | second     |
| The Last Samurai             | Tom Cruise            | premier    |
| Tonnere sous les tropiques   | Ben Stiller           | apparition |
| Tonnere sous les tropiques   | Jack Black            | premier    |
| Tonnere sous les tropiques   | Robert Downer jr      | second     |
| Woman on the beach           | Tom Cruise            | premier    |
+------------------------------+-----------------------+------------+
32 rows in set (0.00 sec)*/


/* REPONSE 3: Listons les differents noms des realisateurs
    et les noms des acteurs par nom de realisateurs et noms d'acteurs dirigeant des films*/

select realisateur.nom_real as realisateur,acteur.nom_act as acteur
     from realisateur
     inner join film on realisateur.num_real = film.num_real
     inner join jouer on film.num_film = jouer.num_film
     inner join acteur on acteur.num_act = jouer.num_act
     ORDER by realisateur, acteur ASC ;

/*Affichage Console 
-> 
+--------------------+-----------------------+
| realisateur        | acteur                |
+--------------------+-----------------------+
| Andreas Dresen     | Angelina Jolie        |
| Andreas Dresen     | Horst Westphal        |
| Andreas Dresen     | Ursula Werner         |
| Anna Novion        | Jean-Pierre Darrousin |
| Anna Novion        | Judith Henry          |
| Antonello Grimaldi | Alessandro Gassman    |
| Antonello Grimaldi | Nanni Moretti         |
| Antonello Grimaldi | Valeria Golino        |
| Ben Stiller        | Ben Stiller           |
| Ben Stiller        | Jack Black            |
| Ben Stiller        | Robert Downer jr      |
| Claude Pinoteau    | Sophie Marceau        |
| Clint Eastwood     | Angelina Jolie        |
| Clint Eastwood     | John Malkovich        |
| Edward Zwick       | Tom Cruise            |
| Hong Sang-soo      | Tom Cruise            |
| Pascal Thomas      | André Dussolier       |
| Pascal Thomas      | Annie Cordy           |
| Pascal Thomas      | Catherine Frot        |
| Pascal Thomas      | Jean-Pierre Darrousin |
| RenÃ© FÃ©ret       | Nicolas Giraud        | 
| RenÃ© FÃ©ret       | Salomé Stéverin       |
| RenÃ© FÃ©ret       | Tom Cruise            |
| Sylvie Verheyde    | Benjamin Biolay       |
| Sylvie Verheyde    | Guillaume Depardieu   |
| Sylvie Verheyde    | Judith Henry          |
| Sylvie Verheyde    | Karole Rocher         |
| Sylvie Verheyde    | Leora Barbara         |
| Sylvie Verheyde    | Melissa Rodrigues     |
| Ursula Meier       | Isabelle Huppert      |
| Ursula Meier       | Jean-Pierre Darrousin |
| Ursula Meier       | Olivier Gourmet       |
+--------------------+-----------------------+
32 rows in set (0.00 sec)
*/

/* REPONSE 4: Listons les numeros, titre et buget des films dans lesquels ont jouée des acteurs 
    nées aprèes 1950*/

select film.num_film,film.titre_film, film.bud_get_film 
    from film
    inner join jouer on film.num_film = jouer.num_film
    inner join acteur on acteur.num_act = jouer.num_act
    where acteur.anNais_act > 1950 
    ORDER BY film.titre_film ASC;

/* Affichage Console : 
    
+----------+------------------------------+--------------+
| num_film | titre_film                   | bud_get_film |
+----------+------------------------------+--------------+
|       10 | Caos Calmo                   |      1000000 |
|       10 | Caos Calmo                   |      1000000 |
|       10 | Caos Calmo                   |      1000000 |
|        6 | Comme une ?oile dans la nuit |      6000000 |
|        6 | Comme une ?oile dans la nuit |      6000000 |
|        6 | Comme une ?oile dans la nuit |      6000000 |
|        7 | Home                         |      3000000 |
|        7 | Home                         |      3000000 |
|        7 | Home                         |      3000000 |
|        9 | L'?hange                     |      5000000 |
|        9 | L'?hange                     |      5000000 |
|       12 | La boom                      |      4000000 |
|        3 | Le crime est notre affaire   |      1000000 |
|        3 | Le crime est notre affaire   |      1000000 |
|        5 | Le septieme ciel             |      2000000 |
|       11 | Les grandes personnes        |      1000000 |
|       11 | Les grandes personnes        |      1000000 |
|        4 | Stella                       |      4000000 |
|        4 | Stella                       |      4000000 |
|        4 | Stella                       |      4000000 |
|        4 | Stella                       |      4000000 |
|        4 | Stella                       |      4000000 |
|        4 | Stella                       |      4000000 |
|        1 | The Last Samurai             |      8000000 |
|        8 | Tonnere sous les tropiques   |      6000000 |
|        8 | Tonnere sous les tropiques   |      6000000 |
|        8 | Tonnere sous les tropiques   |      6000000 |
|        2 | Woman on the beach           |      9000000 |
+----------+------------------------------+--------------+
28 rows in set (0.00 sec)
 ====Raison des repetitions des films ==
    -> La raison dans laquelle un film est jouer plusieurs fois , 
c'est que dans notre schemas MPD, un acteur peut jouer sur plusieurs films 
et un films peut avoir plusieurs acteur.

    !!! Recctifions la raquette !!!*/

select distinct film.num_film,film.titre_film, film.bud_get_film 
    from film
    inner join jouer on film.num_film = jouer.num_film
    inner join acteur on acteur.num_act = jouer.num_act
    where acteur.anNais_act > 1950 
    ORDER BY film.titre_film ASC;

/*Affichage Console : 
    ->  
+----------+------------------------------+--------------+
| num_film | titre_film                   | bud_get_film |
+----------+------------------------------+--------------+
|       10 | Caos Calmo                   |      1000000 |
|        6 | Comme une ?oile dans la nuit |      6000000 |
|        7 | Home                         |      3000000 |
|        9 | L'?hange                     |      5000000 |
|       12 | La boom                      |      4000000 |
|        3 | Le crime est notre affaire   |      1000000 |
|        5 | Le septieme ciel             |      2000000 |
|       11 | Les grandes personnes        |      1000000 |
|        4 | Stella                       |      4000000 |
|        1 | The Last Samurai             |      8000000 |
|        8 | Tonnere sous les tropiques   |      6000000 |
|        2 | Woman on the beach           |      9000000 |
+----------+------------------------------+--------------+
12 rows in set (0.00 sec)*/


/*REPONSE 5: Listons les noms des acteurs qui sont aussi realisateurs 
    portant le meme nom et le meme prenom */

     select acteur.nom_act as acteurRealisateur
     from realisateur 
     inner join acteur on acteur.nom_act = realisateur.nom_real;

    /*Affichage Console :
    -> 
    +-------------------+
    | acteurRealisateur |
    +-------------------+
    | Ben Stiller       |
    | Clint Eastwood    |
    +-------------------+
    2 rows in set (0.00 sec)*/


 /* === PARTIE 2.2 utilisations des Fonctions  === 
  REPONSE 1 : Affichage de la date du jour décalé de 5 jours  */

    SELECT  now() as Aujourdhui, 
    DATE_ADD(now(),INTERVAL 5 DAY) as plus5jours;

  /* Affichage Console :
    -> 
    +---------------------+---------------------+
    | Aujourdhui          | plus5jours          |
    +---------------------+---------------------+
    | 2022-11-30 13:25:02 | 2022-12-05 13:25:02 |
    +---------------------+---------------------+
    1 row in set (0.00 sec)*/

    /* REPONSE 2 : Affichage de la date du jour 
        et la date decalée de 5 mois */

    select now() as Aujourdhui,
    DATE_ADD(now(), INTERVAL 5 month) as plus5mois;

/* Affichage Console : 
+---------------------+---------------------+
| Aujourdhui          | plus5mois           |
+---------------------+---------------------+
| 2022-11-30 13:39:53 | 2023-04-30 13:39:53 |
+---------------------+---------------------+
1 row in set (0.00 sec)*/

/* REPONSE 3: Affichage de la date et du jour et 
    celui decalée de 5 ans*/

    select now() as aujourhui, 
    DATE_ADD(now(), INTERVAL 5 YEAR) as plus5ans;
/* Affichage Console :
+---------------------+---------------------+
| aujourhui           | plus5ans            |
+---------------------+---------------------+
| 2022-11-30 13:56:22 | 2027-11-30 13:56:22 |
+---------------------+---------------------+
1 row in set (0.00 sec)*/

/* REPONSE 4: Listons les dates de projections declaées de 5 mois*/

    select  projection.date_projection, 
    DATE_ADD(date_projection, INTERVAL 5 month) 
    as plus5mois  from projection;
/*Affichage Console :  
+-----------------+------------+
| date_projection | plus5mois  |
+-----------------+------------+
| 2017-09-11      | 2018-02-11 |
| 2017-09-11      | 2018-02-11 |
| 2017-09-12      | 2018-02-12 |
| 2017-09-12      | 2018-02-12 |
| 2017-09-12      | 2018-02-12 |
| 2017-09-12      | 2018-02-12 |
| 2017-09-12      | 2018-02-12 |
| 2017-09-12      | 2018-02-12 |
| 2017-09-13      | 2018-02-13 |
| 2017-09-13      | 2018-02-13 |
| 2017-09-14      | 2018-02-14 |
| 2017-09-14      | 2018-02-14 |
| 2017-09-14      | 2018-02-14 |
| 2017-09-17      | 2018-02-17 |
| 2017-09-17      | 2018-02-17 |
| 2017-09-20      | 2018-02-20 |
| 2017-09-27      | 2018-02-27 |
| 2017-10-01      | 2018-03-01 |
+-----------------+------------+
18 rows in set (0.00 sec)*/

/* REPONSE 5 : Listons les ecarts entres les dates de projection
    et aujour'duis */

    select projeter.date_projection,now(), 
    DATEDIFF(now(), projeter.date_projection)
     as ecart from projeter;
/* Affichage Console : 
+-----------------+---------------------+-------+
| date_projection | now()               | ecart |
+-----------------+---------------------+-------+
| 2017-09-11      | 2022-11-30 13:49:19 |  1906 |
| 2017-09-11      | 2022-11-30 13:49:19 |  1906 |
| 2017-09-11      | 2022-11-30 13:49:19 |  1906 |
| 2017-09-12      | 2022-11-30 13:49:19 |  1905 |
| 2017-09-12      | 2022-11-30 13:49:19 |  1905 |
| 2017-09-12      | 2022-11-30 13:49:19 |  1905 |
| 2017-09-12      | 2022-11-30 13:49:19 |  1905 |
| 2017-09-12      | 2022-11-30 13:49:19 |  1905 |
| 2017-09-12      | 2022-11-30 13:49:19 |  1905 |
| 2017-09-13      | 2022-11-30 13:49:19 |  1904 |
| 2017-09-13      | 2022-11-30 13:49:19 |  1904 |
| 2017-09-14      | 2022-11-30 13:49:19 |  1903 |
| 2017-09-14      | 2022-11-30 13:49:19 |  1903 |
| 2017-09-14      | 2022-11-30 13:49:19 |  1903 |
| 2017-09-14      | 2022-11-30 13:49:19 |  1903 |
| 2017-09-17      | 2022-11-30 13:49:19 |  1900 |
| 2017-09-17      | 2022-11-30 13:49:19 |  1900 |
| 2017-09-20      | 2022-11-30 13:49:19 |  1897 |
| 2017-09-20      | 2022-11-30 13:49:19 |  1897 |
| 2017-09-20      | 2022-11-30 13:49:19 |  1897 |
| 2017-09-27      | 2022-11-30 13:49:19 |  1890 |
| 2017-09-27      | 2022-11-30 13:49:19 |  1890 |
| 2017-10-01      | 2022-11-30 13:49:19 |  1886 |
| 2017-10-01      | 2022-11-30 13:49:19 |  1886 |
| 2017-10-01      | 2022-11-30 13:49:19 |  1886 |
| 2017-10-01      | 2022-11-30 13:49:19 |  1886 |
| 2017-10-01      | 2022-11-30 13:49:19 |  1886 |
+-----------------+---------------------+-------+
27 rows in set (0.00 sec)*/

/* REPONSE 6 : Cherchons le plus grand écart entre 
une date de projection et ajourdhuis*/

select 
    max(DATEDIFF(now(), projection.date_projection))
     as ecartMax from projection;
/* Affichage Console: 
 ->  
 +----------+
| ecartMax |
+----------+
|     1906 |
+----------+
1 row in set (0.00 sec)*/

/*REPONSE 7 :Listons le jours de la semaine
 le jours du mois, le mois de l'annéeet l'année 
 de la date du jour*/

 select DAYOFWEEK(now()) as jourSemaine, 
 dayofmonth(now()) as jourDuMois,month(now())as moisAnne, year(now()) as annee;

/* Affichage Console : 
  -> 
+-------------+------------+----------+-------+
| jourSemaine | jourDuMois | moisAnne | annee |
+-------------+------------+----------+-------+
|           4 |         30 |       11 |  2022 |
+-------------+------------+----------+-------+
1 row in set (0.00 sec)
        1 row in set (0.02 sec)*/

/*REPONSE 8: Listons les acteurs qui ont puis voir un jeudi 
    dans une salle de plus de 300place */

select acteur.num_act,nom_act,anNais_act, weekday(projeter.date_projection) as jourSemaine
from acteur 
inner join jouer on acteur.num_act = jouer.num_act
inner join film on film.num_film = jouer.num_film
inner join projeter on film.num_film = projeter.num_film
inner join salle on salle.num_salle = projeter.num_salle where salle.nbPlace_salle > 300 
and weekday(projeter.date_projection)=3 
order by nom_act ASC;

/*Affichage en Console : 

+---------+---------------------+------------+-------------+
| num_act | nom_act             | anNais_act | jourSemaine |
+---------+---------------------+------------+-------------+
|      22 | Benjamin Biolay     |       1973 |           3 |
|      23 | Guillaume Depardieu |       1971 |           3 |
|       3 | Judith Henry        |       1968 |           3 |
|      20 | Karole Rocher       |       1974 |           3 |
|      19 | Leora Barbara       |       1996 |           3 |
|      21 | Melissa Rodrigues   |       1988 |           3 |
+---------+---------------------+------------+-------------+
6 rows in set (0.00 sec)*/

/*REPONSE 9 : Listons les titres des films, le nom de leur realisateurs et 
    leurs année de naissance,classée par année, 
    nom du realisateur et puis par titre du film*/

    select film.titre_film,realisateur.nom_real,COALESCE(realisateur.anNais_real, '**non fournie**') as neLe
    from film
    inner join realisateur on realisateur.num_real = film.num_real
    order by realisateur.anNais_real,titre_film,nom_real ASC;

/*Affichage Console 

+------------------------------+--------------------+-----------------+
| titre_film                   | nom_real           | neLe            |
+------------------------------+--------------------+-----------------+
| Indianna jones4              | Georges Lucas      | **non fournie** |
| red Tails                    | Georges Lucas      | **non fournie** |
| Star wars                    | Georges Lucas      | **non fournie** |
| La boom                      | Claude Pinoteau    | 1925            |
| L'?hange                     | Clint Eastwood     | 1930            |
| Comme une ?oile dans la nuit | RenÃ© FÃ©ret       | 1945            |
| Le crime est notre affaire   | Pascal Thomas      | 1945            |
| The Last Samurai             | Edward Zwick       | 1952            |
| Caos Calmo                   | Antonello Grimaldi | 1955            |
| Woman on the beach           | Hong Sang-soo      | 1960            |
| Le septieme ciel             | Andreas Dresen     | 1963            |
| Tonnere sous les tropiques   | Ben Stiller        | 1965            |
| Stella                       | Sylvie Verheyde    | 1967            |
| Home                         | Ursula Meier       | 1971            |
| Les grandes personnes        | Anna Novion        | 1979            |
+------------------------------+--------------------+-----------------+
15 rows in set (0.00 sec)*/

/*REPONSE 10: Listons les noms d'acteurs par par sonorisation*/

select nom_act from acteur
WHERE SOUNDEX(nom_act) = SOUNDEX('bnstlr') or SOUNDEX(nom_act) = SOUNDEX('isbldjn') ; 

/*Affichage Console 
    
+-----------------+
| nom_act         |
+-----------------+
| Ben Stiller     |
| Isabelle Adjani |
+-----------------+
2 rows in set (0.00 sec)*/


/* REPONSE 11 : Listons le planning des projections du mois de septembre 2017,
         classé par numero du jour de la semaine, salle, date et heure*/

    SELECT  
    CASE 
      WHEN weekday(projeter.date_projection)=0 THEN 'lundi'
      WHEN weekday(projeter.date_projection)=1 THEN 'mardi'
      WHEN weekday(projeter.date_projection)=2 THEN 'mercredi'
      WHEN weekday(projeter.date_projection)=3 THEN 'jeudi'
      WHEN weekday(projeter.date_projection)=4 THEN 'vendredi'
      WHEN weekday(projeter.date_projection)=5 THEN 'samedi'
      else   'dimanche'

    END   as jour ,CONCAT('salle ',salle.num_salle ) as Salle,
    DATE_FORMAT(projeter.date_projection, '%d %M %Y') as le, projeter.heure_projection as a,
    film.titre_film
    
    from film inner join projeter on  film.num_film = projeter.num_film
    inner join salle on salle.num_salle = projeter.num_salle
    where projeter.date_projection between '2017-09-01' and '2017-09-30' 
    ORDER BY weekday(projeter.date_projection), salle.num_salle, projeter.date_projection
    , projeter.heure_projection ASC;

/*Affichage Console *

    
+----------+---------+-------------------+----------+----------------------------+
| jour     | Salle   | le                | a        | titre_film                 |
+----------+---------+-------------------+----------+----------------------------+
| lundi    | salle 1 | 11 September 2017 | 18:00:00 | Caos Calmo                 |
| lundi    | salle 2 | 11 September 2017 | 18:00:00 | Caos Calmo                 |
| lundi    | salle 3 | 11 September 2017 | 20:20:00 | Caos Calmo                 |
| mardi    | salle 1 | 12 September 2017 | 14:00:00 | L'?hange                   |
| mardi    | salle 1 | 12 September 2017 | 17:45:00 | Woman on the beach         |
| mardi    | salle 2 | 12 September 2017 | 17:00:00 | L'?hange                   |
| mardi    | salle 3 | 12 September 2017 | 20:00:00 | L'?hange                   |
| mardi    | salle 4 | 12 September 2017 | 12:05:00 | Le crime est notre affaire |
| mardi    | salle 4 | 12 September 2017 | 16:20:00 | Le crime est notre affaire |
| mercredi | salle 1 | 13 September 2017 | 18:30:00 | Les grandes personnes      |
| mercredi | salle 1 | 20 September 2017 | 18:15:00 | Le septieme ciel           |
| mercredi | salle 2 | 13 September 2017 | 20:20:00 | Les grandes personnes      |
| mercredi | salle 2 | 20 September 2017 | 18:15:00 | Le septieme ciel           |
| mercredi | salle 3 | 20 September 2017 | 18:15:00 | Tonnere sous les tropiques |
| mercredi | salle 4 | 27 September 2017 | 20:10:00 | Home                       |
| mercredi | salle 5 | 27 September 2017 | 20:10:00 | Home                       |
| jeudi    | salle 3 | 14 September 2017 | 18:00:00 | Stella                     |
| jeudi    | salle 4 | 14 September 2017 | 18:00:00 | Stella                     |
| jeudi    | salle 5 | 14 September 2017 | 20:10:00 | Stella                     |
| jeudi    | salle 5 | 14 September 2017 | 22:00:00 | Le crime est notre affaire |
| dimanche | salle 4 | 17 September 2017 | 17:30:00 | L'?hange                   |
| dimanche | salle 5 | 17 September 2017 | 20:15:00 | L'?hange                   |
+----------+---------+-------------------+----------+----------------------------+
22 rows in set (0.00 sec)*/

/* PARTIE 3 : REJOUER LE SCRIPT 

 Reponse 3.1/2/3 Creation du fichierResultat.txt
 
 Commande->  mysql -u root -h 127.0.0.1 -p < "E:\ETUDES_SECONDAIRES\CM_TP_L2_INFO\SGBDR\TP3\bdCinema.sql" > fichierResultat.txt
 */

 /* PARTIE 4.1 : SAUVAGARDE EN MODE COMMANDE
    
    Commande ->  mysqldump cinema -u root -h 127.0.0.1 -p > sauvBase.sql
    */


/* RESTAURATION EN MODE COMMANDE 

 Commande ->  mysql -h root -u 127.0.0.1 -p > sauvBase.sql*/















