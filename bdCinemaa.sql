/* Nom: MOUAMMAR 
   Prenom :SOULE
   Classe : L2 Info
   Exo 2 TP1 : Grpe 2*/

   /* Creation de la base 
   de donnée Cinema*/
   
drop DATABASE if EXISTS cinema;
CREATE DATABASE if not EXISTS cinema;
use cinema;

/* Creation des Tables
"drop table if EXISTS "Nom_table" : suprimer la base de donnée s'il existe au cours 
        de l'exucution pour eviter les messages d'erreurs
"CREATE TABLE if not EXISTS "Nom_table" : crrer la tables s'elle existe pas

Creation de la table Acteur*/
drop table if EXISTS acteur;
CREATE TABLE if not EXISTS acteur (
    num_act int not null UNIQUE,
    nom_act varchar(40) not null,
    anNais_act int ,
    constraint PK_acteur PRIMARY key (num_act)
) ENGINE=INNODB;

/* Affichae console :
mysql> desc acteur;
+------------+-------------+------+-----+---------+-------+
| Field      | Type        | Null | Key | Default | Extra |
+------------+-------------+------+-----+---------+-------+
| num_act    | int(11)     | NO   | PRI | NULL    |       |
| nom_act    | varchar(40) | NO   |     | NULL    |       |
| anNais_act | int(11)     | YES  |     | NULL    |       |
+------------+-------------+------+-----+---------+-------+



Creation de la table FILM*/
drop table if EXISTS film ;
create TABLE if not EXISTS film(
     num_film int not null UNIQUE,
    num_real int not null,
    titre_film VARCHAR(40),
     anSortie_film int,
     bud_get_film int,
    genre_film VARCHAR(40),
    resum_fil text(2000),
     constraint PK_film  PRIMARY key(num_film)
)ENGINE=INNODB;
/* Affichage Console :
mysql> desc film;
+---------------+-------------+------+-----+---------+-------+
| Field         | Type        | Null | Key | Default | Extra |
+---------------+-------------+------+-----+---------+-------+
| num_film      | int(11)     | NO   | PRI | NULL    |       |
| num_real      | int(11)     | NO   |     | NULL    |       |
| titre_film    | varchar(40) | YES  |     | NULL    |       |
| anSortie_film | int(11)     | YES  |     | NULL    |       |
| bud_get_film  | int(11)     | YES  |     | NULL    |       |
| genre_film    | varchar(40) | YES  |     | NULL    |       |
| resum_fil     | text        | YES  |     | NULL    |       |
+---------------+-------------+------+-----+---------+-------+



Creation de la table realisateur du REALISATEUR*/

drop table if EXISTS realisateur;
create table if not EXISTS realisateur(
    num_real int not null UNIQUE,
    nom_real VARCHAR(40),
    anNais_real int ,
    CONSTRAINT PK_real PRIMARY key(num_real)
) ENGINE=INNODB;

/* Affichage Console :
mysql> desc realisateur;
+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| num_real    | int(11)     | NO   | PRI | NULL    |       |
| nom_real    | varchar(40) | YES  |     | NULL    |       |
| anNais_real | int(11)     | YES  |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+



Creation de la table salle*/

drop table if EXISTS salle;
create table if not EXISTS salle(
    num_salle int not null UNIQUE,
    nbPlace_salle int not null,
    CONSTRAINT PK_salle PRIMARY key(num_salle)
) ENGINE=INNODB;

/* Affichage Console :
mysql> desc salle;
+---------------+---------+------+-----+---------+-------+
| Field         | Type    | Null | Key | Default | Extra |
+---------------+---------+------+-----+---------+-------+
| num_salle     | int(11) | NO   | PRI | NULL    |       |
| nbPlace_salle | int(11) | NO   |     | NULL    |       |
+---------------+---------+------+-----+---------+-------+



Creation de la table PROJECTION */

drop table if EXISTS projection;
create Table if not EXISTS projection(
    date_projection date,
    heure_projection TIME,
    tarif_projection DECIMAL(4,2),
    CONSTRAINT PK_projection PRIMARY key (date_projection,heure_projection)
)ENGINE=INNODB;

/*  Affichage Console :
mysql> desc projection;
+------------------+--------------+------+-----+---------+-------+
| Field            | Type         | Null | Key | Default | Extra |
+------------------+--------------+------+-----+---------+-------+
| date_projection  | date         | NO   | PRI | NULL    |       |
| heure_projection | time         | NO   | PRI | NULL    |       |
| tarif_projection | decimal(4,2) | YES  |     | NULL    |       |
+------------------+--------------+------+-----+---------+-------+



Creation de la table jouer*/

drop table if EXISTS jouer;
create table if not EXISTS jouer(
    num_act int not null,
    num_film int not null,
    role_joueur varchar(40),
    cachet_jouer int,
    constraint PK_jouer PRIMARY key(num_act,num_film)
)ENGINE=INNODB;

/* Affichage Console:
mysql> desc jouer;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| num_act      | int(11)     | NO   | PRI | NULL    |       |
| num_film     | int(11)     | NO   | PRI | NULL    |       |
| role_joueur  | varchar(40) | YES  |     | NULL    |       |
| cachet_jouer | int(11)     | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+



Creation de la table PROJETER*/

drop table if EXISTS projeter;
create table if not EXISTS projeter(
    num_salle int not null,
    num_film int not null,
    date_projection date,
    heure_projection time,
    nbSpectateur_proj int,
    CONSTRAINT PK_projeter PRIMARY key(num_salle,num_film,date_projection,heure_projection)
)ENGINE=INOODB;

/* Affichage Console:
mysql> desc projeter;
+-------------------+---------+------+-----+---------+-------+
| Field             | Type    | Null | Key | Default | Extra |
+-------------------+---------+------+-----+---------+-------+
| num_salle         | int(11) | NO   | PRI | NULL    |       |
| num_film          | int(11) | NO   | PRI | NULL    |       |
| date_projection   | date    | NO   | PRI | NULL    |       |
| heure_projection  | time    | NO   | PRI | NULL    |       |
| nbSpectateur_proj | int(11) | YES  |     | NULL    |       |
+-------------------+---------+------+-----+---------+-------+

*/


/*Ajout des constraint de referentielles(clées etrangeres) dans les tables */
alter table jouer 
    add CONSTRAINT FK_acteur_jouer Foreign Key (num_act) REFERENCES acteur(num_act)
    on UPDATE CASCADE
    on delete CASCADE;


alter table jouer 
    add CONSTRAINT FK_film_jouer Foreign Key (num_film) REFERENCES film(num_film)
    on UPDATE CASCADE
    on delete CASCADE;


alter table projeter 
    add CONSTRAINT FK_projection_projeter FOREIGN key (date_projection,heure_projection) 
    REFERENCES projection(date_projection,heure_projection)
    on UPDATE CASCADE
    on DELETE CASCADE;


alter table projeter 
    add CONSTRAINT FK_salle_projeter FOREIGN key (num_salle) 
    REFERENCES salle(num_salle)
    on UPDATE CASCADE
    on DELETE CASCADE;


alter table projeter 
    add CONSTRAINT FK_film_projeter FOREIGN key (num_film) 
    REFERENCES film(num_film)
    on UPDATE CASCADE
    on DELETE CASCADE;

alter table film 
    add CONSTRAINT FK_realisateur_film FOREIGN key (num_real) 
    REFERENCES realisateur(num_real)
    on UPDATE CASCADE
    on DELETE CASCADE;



/*REPONSES AUX QUESTIONS

REPONSE 1: Insertion des lignes en tenant compte des elements suivants avec la salle 4 sans nombre de classe*/

insert into salle(num_salle,nbPlace_Salle)
    VALUES(1,100);

insert into salle(num_salle,nbPlace_Salle)
    VALUES(2,200);
/*
insert into salle(num_salle,nbPlace_Salle)
    VALUES(4, );
    ->Affichage au console de la requette si-dessus :[Le champ 'nbPlace_salle' ne peut être vide (null)]
 Impossible d'jaouter la salle 4 sans nombre de place 
 car selon l'exercice le nombre de place est un entier requis not null,
 */


 /* Inserons la 4 salle avec un nombre  de 300 places */

 insert into salle(num_salle,nbPlace_Salle) VALUES(4,300);

    /* Resulatat 
    mysql> select *from salle;
    +-----------+---------------+
    | num_salle | nbPlace_salle |
    +-----------+---------------+
    |         4 |           300 |
    +-----------+---------------+ 
   

/*Ajoutons une salle sans numero de salle de 400 places 
 Requette -> :  insert into salle(num_salle,nbPlace_Salle) VALUES (400); 
 ->Afichae console :
ERROR 1366 (HY000): Incorrect integer value: ' ' for column 'num_salle' at row 1
-> il est impossible car la clée primaire num salle ne peux pas etre null*/



/* Ajoutons une salle 5 de salle de 400 places
    Ajoutons une salle 6 de salle de 99999999999 places
    Ajoutons une salle 6 de salle de 500 places
 */
 insert into salle(num_salle,nbPlace_Salle) VALUES (5,400);
 insert into salle(num_salle,nbPlace_Salle) VALUES (6,500);
/*-> insert into salle(num_salle,nbPlace_Salle) VALUES (6,99999999999);
 Requette erreur car la valeur de la salle 
est en dehors du champs int en octet
Affichage console : ->  ERROR 1264 (22003): Out of range value for column 'nbPlace_salle' at row 1


 /* Reponse 2 : requette de verification :*/
 select *from salle;
 /*
+-----------+---------------+
| num_salle | nbPlace_salle |
+-----------+---------------+
|         1 |           100 |
|         2 |           200 |
|         4 |           300 |
|         5 |           400 |
|         6 |           500 |
+-----------+---------------+
-> Reponse au console : on remarque que certains donnée ne sont pas la et d'autre sont 
 ecrasé a cause de la clée primaire et du champs nombre_place initialement requis donc ne peux pas 
 etre null et d'autre en dehors du type de données en memoire.


Reponse 3_a :MODIFIACTION DE LA SALLE 1 EN 150 PLACES */

UPDATE salle
    set nbPlace_salle = 150 where num_salle = 1;

/* REPONSE 3_b: selon le console impossible 
de mettre le champ Nombre de place à NULL
car c'est une entier requis ,not null et donc peux pas etre null
sauf changer le type or c'est pas 
ce qui été demandé

update salle 
    set nbPlace_salle = NULL where num_salle = 2;



/* REponse 3_c:Ajoutons 50 place a tous les 
les salles sans la salle 1*/

UPDATE salle 
   set nbPlace_salle = nbPlace_salle + 50 where num_salle in(2,4,5,6);


/* REponse 4:Verifications des donées en midifications
par un requttes sql */
select *from salle;

/* Affichage console : on remarque bien que chaque salle
a eu une augmentation de 50 places
+-----------+---------------+
| num_salle | nbPlace_salle |
+-----------+---------------+
|         1 |           150 |
|         2 |           250 |
|         4 |           350 |
|         5 |           450 |
|         6 |           550 |
+-----------+---------------+*/

/* Reponse 5 : Suppression des salles 
    qui ont plus de 300 places*/
DELETE from salle where nbPlace_salle > 300;


/* Reponse 6 :Verifions si la requitte 
    precendente a fonction 
*/
select *from salle;
/* affichage console 
+-----------+---------------+
| num_salle | nbPlace_salle |
+-----------+---------------+
|         1 |           150 |
|         2 |           250 |
+-----------+---------------+
*/

/* Reponse 7: supressions de toutes les salles */
DELETE from salle where nbPlace_salle < 300;

/* Verification */
select *from salle;

/* Affichage console
Query OK, 2 rows affected (0.02 sec)
Empty set (0.00 sec) */






